bibhtml:
	cd biblio ; bibtex2html -s ../report/naereen.bst -u -charset utf-8 -linebreak *.bib ; cd ..

clean:
	cd report ; make clean ; cd ..
	cd slides ; make clean ; cd ..

stats:
	+git-complete-stats.sh | tee ./complete-stats.txt
	git wdiff ./complete-stats.txt

cloudwords:
	-generate-word-cloud.py -s -m 75 -t "Words from LaTeX sources - Neuro-Science project - (C) 2016 Lilian Besson" ./*.md ./*.txt */*.md {slides,report}/*.tex */*.py
	generate-word-cloud.py -f -o cloudwords.png -m 75 -t "Words from LaTeX sources - Neuro-Science project - (C) 2016 Lilian Besson" ./*.md ./*.txt */*.md {slides,report}/*.tex */*.py
