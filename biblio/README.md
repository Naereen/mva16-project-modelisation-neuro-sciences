## References
- [This BibTeX](./mva16-project-modelisation-neuro-sciences.bib) file listed all the references we used for our project;
- [See it as plain text (.txt)](./mva16-project-modelisation-neuro-sciences.txt);
- Or [it as HTML](./mva16-project-modelisation-neuro-sciences.html) or [as PDF](https://bitbucket.org/lbesson/mva16-project-modelisation-neuro-sciences/downloads/MVA16__Neuro-Science_Project__References__Lilian_Besson__03-16.en.pdf);

- The references are used in [the report](../report/) and [the slides](../slides/).

----

#### More ?
- See [the report](../report/) for more details about the models.
- See [the code](../src/) or [the figures](../fig/) if needed.

> - [MIT Licensed](../LICENSE)!
> - [Go back to the main folder?](../)
