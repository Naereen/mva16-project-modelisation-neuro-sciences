#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
================================
Color Quantization using K-Means
================================

Performs a pixel-wise Vector Quantization (VQ) of a HD photo (from Iceland),
reducing the number of colors required to show the image from 96,615
unique colors to 64, while preserving the overall appearance quality.

In this example, pixels are represented in a 3D-space and K-means is used to
find 64 or 32 color clusters. In the image processing literature, the codebook
obtained from K-means (the cluster centers) is called the color palette. Using
a single byte, up to 256 colors can be addressed, whereas an RGB encoding
requires 3 bytes per pixel. The GIF file format, for example, uses such a
palette.

For comparison, a quantized image using a random codebook (colors picked up
randomly) is also shown.

http://scikit-learn.org/dev/auto_examples/cluster/plot_color_quantization.html
"""
# Authors: Robert Layton <robertlayton@gmail.com>
#          Olivier Grisel <olivier.grisel@ensta.org>
#          Mathieu Blondel <mathieu@mblondel.org>
#
# License: BSD 3 clause

print(__doc__)
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.metrics import pairwise_distances_argmin
from sklearn.utils import shuffle
from time import time


def savefig(name, *args, **kwargs):
    """ Print an info message and save the figure. """
    print("Saving the figure to '{}' ...".format(name))
    # plt.savefig(name, *args, **kwargs)
    plt.savefig(name, *args, dpi=280, **kwargs)


def recreate_image(codebook, labels, w, h):
    """ Recreate the (compressed) image from the code book & labels.
    """
    d = codebook.shape[1]
    image = np.zeros((w, h, d))
    label_idx = 0
    for i in range(w):
        for j in range(h):
            image[i][j] = codebook[labels[label_idx]]
            label_idx += 1
    return image


def main(n_colors=32):
    """ Run the main experiment, with target n_colors for the quantization.
    """
    print("Reducing to {} colors.".format(n_colors))

    USE_CHINA = False
    if USE_CHINA:
        from sklearn.datasets import load_sample_image
        # Load the Summer Palace photo
        photoname = "china.jpg"
        myphoto = load_sample_image(photoname)
    else:
        photoname = "Heamey.jpg"
        myphoto = plt.imread(photoname)

    print("Loading the '{}' image... shape {} ...".format(photoname, np.shape(myphoto)))

    # Convert to floats instead of the default 8 bits integer coding. Dividing by
    # 255 is important so that plt.imshow behaves works well on float data (need to
    # be in the range [0-1]
    myphoto = np.array(myphoto, dtype=np.float64) / 255

    # Load Image and transform to a 2D numpy array.
    w, h, d = original_shape = tuple(np.shape(myphoto))
    assert d == 3, "Error, the image is not a 3-colors one."
    image_array = np.reshape(myphoto, (w * h, d))
    print("The input image as w = {} x h = {} = {} pixels ...".format(w, h, w * h))

    nb_input_colors = len(set((m[0], m[1], m[2]) for m in image_array))
    # 96615 for 'china.jpg', 75986 for 'Heamey.jpg'
    print("The input image had", nb_input_colors, "unique colors ...")

    # Fit and predict the K-Means model
    print("Fitting model on a small sub-sample of the data ...")
    t0 = time()
    image_array_sample = shuffle(image_array, random_state=0)[:1000]
    kmeans = KMeans(n_clusters=n_colors, random_state=0).fit(image_array_sample)
    print("  Done in %0.3fs ..." % (time() - t0))

    # Get labels for all points
    print("Predicting color indices on the full image (k-means) ...")
    t0 = time()
    labels_kmeans = kmeans.predict(image_array)
    codebook_kmeans = kmeans.cluster_centers_
    print("  Done in %0.3fs ..." % (time() - t0))

    # Random codebook
    codebook_random = shuffle(image_array, random_state=0)[:n_colors + 1]
    print("Predicting color indices on the full image (random) ...")
    t0 = time()
    labels_random = pairwise_distances_argmin(codebook_random, image_array, axis=0)
    print("  Done in %0.3fs ..." % (time() - t0))

    # Display all results, alongside original image
    t0 = time()
    print("Plotting the original image ...")
    plt.figure(1)
    plt.clf()
    plt.axes([0, 0, 1, 1])
    plt.axis('off')
    plt.title("Original image ({} colors)".format(nb_input_colors))
    plt.imshow(myphoto)
    # plt.show()
    print("  Done in %0.3fs ..." % (time() - t0))

    name = photoname.replace('.jpg', '')

    t0 = time()
    print("Plotting the K-Means quantized image ...")
    plt.figure(2)
    plt.clf()
    plt.axes([0, 0, 1, 1])
    plt.axis('off')
    plt.title("Quantized image ({} colors, K-Means)".format(n_colors))
    plt.imshow(recreate_image(codebook_kmeans, labels_kmeans, w, h))
    # plt.show()
    savefig('../../fig/color-quantization/{}__KMean_quantized__{}_colors.jpg'.format(name, n_colors))
    print("  Done in %0.3fs ..." % (time() - t0))

    t0 = time()
    print("Plotting the random codebook quantized image ...")
    plt.figure(3)
    plt.clf()
    plt.axes([0, 0, 1, 1])
    plt.axis('off')
    plt.title("Quantized image ({} colors, Random)".format(n_colors))
    plt.imshow(recreate_image(codebook_random, labels_random, w, h))
    # plt.show()
    savefig('../../fig/color-quantization/{}__random_quantized__{}_colors.jpg'.format(name, n_colors))
    print("  Done in %0.3fs ..." % (time() - t0))

    # TODO
    # t0 = time()
    # print("Plotting the Self-Organizing Map codebook quantized image ...")
    # plt.figure(4)
    # plt.clf()
    # plt.axes([0, 0, 1, 1])
    # plt.axis('off')
    # plt.title("Quantized image (64 colors, SOM)")
    # plt.imshow(recreate_image(codebook_som, labels_som, w, h))
    # savefig('../../fig/color-quantization/{}_SOM_quantized__{}_colors.jpg'.format(name, n_colors))
    print("TODO : same quantization but with a Self-Organizing Map instead of K-Means ...")
    plt.show()


if __name__ == '__main__':
    for n_colors in [32, 64]:
        main(n_colors)
    print("Done.")
