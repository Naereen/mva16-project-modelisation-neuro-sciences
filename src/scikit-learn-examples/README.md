### Python examples, from [scikit-learn documentation](http://scikit-learn.org/dev/)
> See <http://scikit-learn.org/dev/auto_examples/> for the list of examples.

- <http://scikit-learn.org/dev/auto_examples/cluster/plot_color_quantization.html>;
- <http://scikit-learn.org/dev/auto_examples/cluster/plot_face_compress.html>.
