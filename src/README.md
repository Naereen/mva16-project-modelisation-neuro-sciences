## Python programs for the Modelisation in Neuro-Sciences course
> - Project by Lilian Besson.
> - Written in [Python](https://www.python.org/), 2 or 3 (I always do my best in supporting both versions).

### [Dependencies](./requirements.txt)
> The following modules were used:

#### Generic scientific modules:
- [numpy](http://www.numpy.org/) and [scipy](http://www.scipy.org/): numpy-array structures, scientific functions on array;
- [matplotlib](http://www.matplotlib.org/): for plotting.

----

### Organization
> - Each of these Python programs is very well documented, with inline comments (``# comments``) and docstring (``""" Documentation. """``).
> - It should be very clear what each variable is for, what each function/method does, and how to use each class.

#### Hand-made modules
> FIXME

#### Scripts
> FIXME

----

### Evaluation of the code quality
[pylint](https://www.pylint.org/) says:

**Your code has been rated at XXX/10**

*That's a pretty good sign that our code is clean and well written*
(not a sign that it works as expected, but the experiments worked as expected, and [the figures and animations](../fig/) seems to indicate that everything works fine).

> [See the complete report](./pylint_report.md) or [each report individually](./pylint/) for more details.


----

### Source of inspiration
> FIXME

----

#### More ?
- See [the report](../report/) for explanations about this programs.
- See [the data](../data/), [the figures](../fig/) or [the references](../biblio/) if needed.

> - [MIT Licensed](../LICENSE)!
> - [Go back to the main folder?](../)
