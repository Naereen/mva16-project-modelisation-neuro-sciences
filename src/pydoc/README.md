## Documentation
> Generated with [pydoc](https://docs.python.org/3/library/pydoc.html) command line program.

### For the programs:

- [for `kmeans.py`](../kmeans.py): documentation on [kmeans.pydoc.txt](./kmeans.pydoc.txt);
