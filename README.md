# [*Modeling in Neuro-Sciences*](http://www.lps.ens.fr/%7Enadal/Cours/MVA/) project (Master MVA)
### Meta-data:
- Subject: **Self-Organizing Maps (SOM) and Dynamic SOM, from unsupervised clustering to models of cortical plasticity**,
- Advisors: [Nicolas P. Rougier](http://www.labri.fr/perso/nrougier/) and [J.-P. Nadal](http://www.lps.ens.fr/%7Enadal/),
- Keywords: SOM, DSOM, Neural Networks, Cortical Plasticity,
- [How to validate the project](http://www.lps.ens.fr/~nadal/Cours/MVA/odp2016.html),
- [Main Reference](http://www.labri.fr/perso/nrougier/coding/article/article.html)
- Where: [git repository on bitbucket.org](https://bitbucket.org/lbesson/mva16-project-modelisation-neuro-sciences/),
- Date: March 2016.

### Feedback:
- **Grade**: I got *17.5*/20 for this course;
- **Rank**: 5th amongst 13th students who got a grade (for them, average was 15.9/20).

### [Quick description](http://www.labri.fr/perso/nrougier/coding/article/article.html):
> We present in this project a variation of the self-organizing map algorithm where the original time-dependent (learning rate and neighborhood) learning function is replaced by a time-invariant one.
> This allows for on-line and continuous learning on both static and dynamic data distributions
> One of the property of the newly proposed algorithm is that it does not fit the magnification law and the achieved vector density is not directly proportional to the density of the distribution as found in most vector quantization algorithms.
> From a biological point of view, this algorithm sheds light on cortical plasticity seen as a dynamic and tight coupling between the environment and the model.

### Teasers:

![Comparison of Neural Gas / SOM / DSOM on a double-ring distribution](./fig/DSOM/double-ring.png "Comparison of Neural Gas / SOM / DSOM on a double-ring distribution")

> Neural Gas / Self-Organizing Map / Dynamic Self-Organizing Map, on a static double-ring distribution (in 2D).


![Comparison of Neural Gas / SOM / DSOM on a dynamic distribution](./fig/DSOM/dynamic.png "Comparison of Neural Gas / SOM / DSOM on a dynamic distribution")

> Neural Gas / Self-Organizing Map / Dynamic Self-Organizing Map, on a dynamic distribution (in 2D, moving from quarters 3 → 2 → 1 → 4).

----

## Things to do
### Main dates (DONE)
- [X] Beginning February: be sure J.-P. Nadal accepts my project (OK);
- [X] February and March: work regularly on the project;
- [X] End of March: conclude the project (code, report, slides);
- [X] 31st of March: oral presentation.

### [Topic](http://www.lps.ens.fr/~nadal/Cours/MVA/odp2016.html)
> *« Self-Organizing Maps (SOM) and Dynamic SOM, from Unsupervised Clustering to Models of Cortical Plasticity »*.

## [Bibliographic references](./biblio/)
> For more details, see [this folder (biblio/)](./biblio/).

### Main references
- ["Dynamic Self-Organizing Map (PDF)"](https://hal.inria.fr/file/index/docid/495827/filename/draft-revised.pdf) or ["Dynamic Self-Organizing Map (Web)"](http://www.labri.fr/perso/nrougier/coding/article/article.html), by N. Rougier and Y.  Boniface (2011);
- ["Slides on Kohonen Maps (French)"](http://www.labri.fr/perso/nrougier/downloads/Kohonen.pdf), by N. Rougier (2013);
- ["Self-Organizing Dynamic Neural Fields"](https://hal.inria.fr/inria-00587508), by N. Rougier and G. Detorakis (2011).

----

## [Code and programs](./src/) (in Python)
- Not a lot of original programs for this project;
- I worked on and with programs from the [reference articles](./biblio/).

----

## Project reports
### [Final report](./report/) (DONE)
Here [is the report](https://bitbucket.org/lbesson/mva16-project-modelisation-neuro-sciences/downloads/MVA__Modelisation_Neuro_Sciences__project__Lilian_Besson__2015-16__Report.en.pdf) (22 pages, *in English*).

### [Final presentation](./slides/) with slides (DONE)
Here [are the slides](https://bitbucket.org/lbesson/mva16-project-modelisation-neuro-sciences/downloads/MVA__Modelisation_Neuro_Sciences__project__Lilian_Besson__2015-16__Slides.en.pdf) (38 slides, *in English*).

- The presentation was on Thursday, 31st of March.
- Oral presentation: 20 min, + 10-15 min of questions (on the presentation and on the course);

----

## About
This project was done for the [*Modeling in Neuro-Sciences*](http://www.lps.ens.fr/%7Enadal/Cours/MVA/) course for the [MVA master program](http://www.cmla.ens-cachan.fr/version-anglaise/academics/mva-master-degree-227777.kjsp) at [ENS de Cachan](http://www.ens-cachan.fr).

![Cloud of words for this project](./cloudwords.png "Cloud of words for this project")

> Cloud of words for the Markdown, LaTeX and Python sources for this project. Made with [``generate-word-cloud.py``](https://github.com/Naereen/generate-word-cloud.py/).

### Copyright
©, 2016, [Lilian Besson](http://perso.crans.org/besson/) ([ENS de Cachan](http://www.ens-cachan.fr)).

### Licence
After the final presentation, the project will be publicly published, under the terms of the [MIT license](http://lbesson.mit-license.org/).
